### Installation procedures

###### Installation procedure (encrypted lvm):

  1. Create efi partition:
       
       `# fdisk /dev/sdX`

          * g (to create an empty GPT partition table)
          * n
          * 1
          * enter
          * +1024M
          * t
          * 1 (For EFI)
          * w

  2. Create boot partition:

       `# fdisk /dev/sdX`

          * n
          * 2
          * enter
          * +1024M
          * w

  3. Create LVM partition:
      
       `# fdisk /dev/sdX`

          * n
          * 3
          * enter
          * enter
          * t
          * 3
          * 31
          * w

  4. `# mkfs.fat -F32 /dev/sdX1`
  5. `# mkfs.ext2 /dev/sdX2`

###### If using disk encryption:

  1. Set up encryption:

          # cryptsetup luksFormat /dev/sdX3
          # cryptsetup open --type luks /dev/sdX3 lvm

###### End If
        
  6. Set up lvm:
        
          # pvcreate (--dataalignment 1m (if installing on SSD)) /dev/mapper/lvm (if using encryption) /dev/sdX3 (if not)
          # vgcreate volgroup0 /dev/mapper/lvm (if usint encryption) /dev/sdX3 (if not)
          # lvcreate -L 50GB volgroup0 -n lv_root
          # lvcreate -l 100%FREE volgroup0 -n lv_home
          # modprobe dm_mod
          # vgscan
          # vgchange -ay
        
  7. `# mkfs.ext4 /dev/volgroup0/lv_root`
  8. `# mkfs.ext4 /dev/volgroup0/lv_home`

###### If installing using wifi:

  1. `# ip a`
  2. `# cp /etc/netctl/examples/wireless-wpa /etc/netctl/wireless`
  3. `# nano /etc/netctl/wireless`
  4. Change `Interface=` with the interface that `ip a` returns

     Change `ESSID=` with your network's SSID

     Change `Key=` with your network's password
  5. `# netctl start wireless`

###### End If

  9. `# mount /dev/volgroup0/lv_root /mnt`
  10. `# mkdir /mnt/boot`
  11. `# mkdir /mnt/home`
  12. `# mount /dev/sdX2 /mnt/boot`
  13. `# mount /dev/volgroup0/lv_home /mnt/home`
  14. `# pacstrap -i /mnt base`
  15. `# genfstab -U -p /mnt >> /mnt/etc/fstab`
  16. `# arch-chroot /mnt`
  17. `# pacman -S grub efibootmgr dosfstools openssh os-prober mtools linux-headers networkmanager network-manager-applet wpa_supplicant wireless_tools wpa_actiond dialog`
  18. Edit `/etc/mkinitcpio.conf` and add `encrypt (if using encryption) lvm2` in between `block` and `filesystems`
  19. `# mkinitcpio -p linux`
  20. `# nano /etc/locale.gen` (uncomment en_US.UTF-8)
  21. `# locale-gen`
  22. Enable `root` logon via `ssh`
  23. `# systemctl enable sshd.service`
  24. `# passwd` (for setting root password)
  25. Edit `/etc/default/grub`:
        add `cryptdevice=<PARTUUID>:volgroup0` to the `GRUB CMDLINE LINUX DEFAULT` line
        If using standard device naming, the option will look like this: `cryptdevice=/dev/mapper/lvm:volgroup0` (if using encryption) or `cryptdevice=/dev/sdX3:volgroup0` (if not)
  26. `# mkdir /boot/EFI`
  27. `# mount /dev/sdX1 /boot/EFI`
  28. `# grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck`
  29. `# cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo`
  30. `# grub-mkconfig -o /boot/grub/grub.cfg`
  31. Create swap file:

          # fallocate -l 4G /swapfile
          # chmod 600 /swapfile
          # mkswap /swapfile
          # echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

  32. `# exit`
  33. `# umount -a`
  34. `# reboot`

### Post Installation

  1. `# pacman -Sy xf86-input-libinput xorg-server xorg-xinit xorg-server-utils mesa pango`